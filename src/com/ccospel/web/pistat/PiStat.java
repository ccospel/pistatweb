package com.ccospel.web.pistat;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class PiStat
 */
@WebServlet("/PiStat")
public class PiStat extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int curTemp;
	private int setTemp;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PiStat() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException {
    	curTemp=42;
    	log("OK!");
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*request.setAttribute("set_temperature", ""+Integer.toString(setTemp));
		request.setAttribute("cur_temperature", ""+Integer.toString(curTemp));
		request.getRequestDispatcher("/WEB-INF/Main.jsp").forward(request, response); */ 
		curTemp++;
		log("doGet: "+Integer.toString(curTemp));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*if (request.getParameter("change").equals("+"))
			setTemp++;
		else
			setTemp--;
		request.setAttribute("set_temperature", ""+Integer.toString(setTemp));
		request.setAttribute("cur_temperature", ""+Integer.toString(curTemp));*/
		//request.getRequestDispatcher("/WEB-INF/Main.jsp").forward(request, response);
		if (curTemp++==100)
			curTemp=0;
		log("doPost: "+Integer.toString(curTemp));
		log(" ["+request.getParameter("Command")+"]");
		
		if (request.getParameter("Command").equals("+"))
			setTemp++;
		else if (request.getParameter("Command").equals("-"))
			setTemp--;
			
		JSONObject jObj = new JSONObject();
		jObj.put("curTemp", Integer.toString(curTemp));
		jObj.put("setTemp", Integer.toString(setTemp));
	
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.println(jObj);
	}
	
	public void destroy () {
		
	}

}
