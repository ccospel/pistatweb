<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- meta http-equiv="Refresh" content="5; url=PiStat" /-->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Raspberry Pi Thermostat</title>

<script src="jquery-2.1.4.min.js"></script>

<style>
#header {
	background-color:blue;
	color:white;
	text-align:center;
	padding:5px
}
#forecast {
	line-height:30px;
	background-color:#eeeeee;
	height:250px;
	float:top;
	padding:5px;
	text-align:center;
}
#section1 {
	width:120px;
	float:right;
	padding:10px;	
	font-family:verdana;
	font-size:300%;
	line-height: 1px;
}
#section2 {
	font-family:verdana;
	font-size:500%;
	width:150px;
	float:right;
	padding:10px;
	line-height: 1px;
}
</style>
</head>
<body>
	<div id="header">
		Main Schedule Weather
	</div>
	<div id="forecast">
		<iframe id="forecast_embed" type="text/html" frameborder="0" height="100%" width="100%" src="http://forecast.io/embed/#lat=37.1411&lon=-80.4078&name=Christiansburg VA"> </iframe>
	</div>
	<div id="section1">
		<center>
		<input id="tempUp" type="image" src="up.png" value="+"/> 
		<p id="sTemp">UNKNOWN</p>
		<input id="tempDown" type="image" src="down.png" value="-"/>
		</center>
	</div>
	<div id="section2">
		<center><b><p id="cTemp">UNKNOWN</p></b></center>
	</div>
	
	
<script type="text/javascript">
$(document).ready(function() {
    $("#tempUp").click(function(){
    	$.post("PiStat", {Command : "+"}, parseResponse);
    });
    $("#tempDown").click(function(){
    	$.post("PiStat", {Command : "-"}, parseResponse);
    });
    updatePost();
});

function parseResponse(response) {
	document.getElementById("sTemp").innerHTML=""+response.setTemp+"&deg;";
	document.getElementById("cTemp").innerHTML=""+response.curTemp+"&deg;";
}

function updatePost() {
	$.post("PiStat", {Command : "update"}, parseResponse);
	
}

var interval = setInterval(updatePost, 5000);
</script>	
</body>
</html>